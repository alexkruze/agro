<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Agro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./public/css/main.css">
</head>
<body>

<div id="app"></div>

	
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="./public/js/bundle.js"></script>
</body>
</html>