var fs = require("fs")
var browserify = require('browserify')
var vueify = require('vueify')
var gulp = require('gulp');
var babelify = require('babelify');
var sass = require('gulp-sass');




gulp.task('vueify', function () {
  return browserify('./src/main.js')
  .transform(babelify, { presets: ['es2015'], plugins: ["transform-runtime"] })
  .transform(vueify)
  .bundle()
  .pipe(fs.createWriteStream("../public/js/bundle.js"))

});

gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('../public/css/'));
});

